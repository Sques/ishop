$(document).ready(function(){
	initMap();
	initCountInput();
	initPlayerForm();
	maskInput();
	initTabs();
	initTabsForm();
	$('.bxslider').bxSlider({
		controls: false,
		auto: true
	});
	$('.products-slider').each(function(){
		$(this).slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			// autoplay: true,
			autoplaySpeed: 3000,
			responsive: [
				{
					breakpoint:1080,
					settings:{
						arrows:false
					}
				},
				{
					breakpoint:850,
					settings:{
						arrows:false,
						slidesToShow: 3,
						autoplay: true,
						autoplaySpeed: 3000,
					}
				},
				{
					breakpoint:550,
					settings:{
						arrows:false,
						slidesToShow: 2,
						autoplay: true,
						autoplaySpeed: 3000,
					}
				},
				{
					breakpoint:390,
					settings:{
						arrows:false,
						slidesToShow: 1,
						autoplay: true,
						autoplaySpeed: 3000,
					}
				}
			]
		});
	});
	$('.partners-slider').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint:850,
				settings:{
					slidesToShow: 3,
					autoplay: true,
					autoplaySpeed: 3000,
				}
			},
			{
				breakpoint:850,
				settings:{
					slidesToShow: 2,
					autoplay: true,
					autoplaySpeed: 3000,
				}
			}
		]
	});
	$('input, textarea').placeholder(); 
	initPopups ();
	function initPopups (){
		$('body')
			.popup({
				"opener":".open-popup",
				"popup_holder":"#popup",
				"popup":".popup",
				"close_btn":".close-popup"
			})
	};
	$('.header .btn-search').click(function(){
		$('.header').addClass('open')
		return false;
	});
	$('.header-form .close').click(function(){
		$('.header').removeClass('open')
		return false;
	});
	$('.header-form .text').focus(function(){
		$('.header-form .help-list').addClass('open');
		return false;
	});
	$('.header-form .text').blur(function(){
		$('.header-form .help-list').removeClass('open');
		return false;
	});
	$('.header .basket .btn-basket').click(function(){
		$('.header .basket').toggleClass('open')
		return false;
	});
	$(document).click(function(event) {
		if ($(event.target).closest('.header .basket').length) return;
		$('.header .basket').removeClass('open')
		event.stopPropagation();
	});
	$(document).click(function(event) {
		if ($(event.target).closest('#nav > ul > li.has-drop2').length) return;
		$('#nav > ul > li.has-drop2').removeClass('open')
		event.stopPropagation();
	});

	$('#nav > ul > li.has-drop').click(function(){
		$('#nav > ul > li.has-drop').toggleClass('open');
		$('.header').toggleClass('big-open');
		return false;
	});
	$('#nav > ul > li.has-drop2').click(function(){
		$('#nav > ul > li.has-drop2').toggleClass('open')
		return false;
	});
	$('.mob-btn').on('click', function() {
		$('.header').toggleClass("catalog-open");
		return false;
	});


	$('.catalog-list li').matchHeight();
	$('.catalog-list h3').matchHeight();
	$('.basket-form .row').matchHeight({
		byRow: false,
	});
	$('.basket-form .content').matchHeight({
		byRow: false,
	});

	$('.card-section .slider .switcher-list  span').matchHeight({
		byRow: false,
	});
	$('.card-section .top-holder > *').matchHeight();


	$('.card-section .about-product .text').hide();
	$('.card-section .about-product .title').click(function(){
		$(this).next().slideToggle(500);
		$('.card-section .about-product').toggleClass('open');
	});


	$('.finally-form .btn-holder .btn.step').on('click', function(){
		if($(this).closest('fieldset').find('input:radio').length){
			var f = false;
			$(this).closest('fieldset').find('input:radio').each(function(){
				if($(this).prop('checked')){
					f = true;
				}
			})
			if (!f){
				$(this).closest('fieldset').addClass('error');
				return false;
			}
		} else {
			$(this).closest('fieldset').find('input').blur();
		}
		if($('.finally-form .error').length){}
		else if ($(this).closest('fieldset').hasClass('active')){
			$(this).closest('fieldset').removeClass('active');
			$(this).closest('fieldset').addClass('finished');
			$(this).closest('fieldset').next().removeClass('finished');
			$(this).closest('fieldset').next().addClass('active');
		};
		$('.finally-form .active.first .btn-holder .btn').val('Далее');
		$('.finally-form .finished.first .btn-holder .btn').val('Изменить данные');
		$('.finally-form .active.second .btn-holder .btn').val('Далее');
		$('.finally-form .finished.second .btn-holder .btn').val('Изменить данные');
		return false;
	});


	$('.checks input').on('change', function(){
		$(this).closest('fieldset').removeClass('error');
		switch($(this).data('order')){
			case 'pickup':
				$('.finally-form input.pay').trigger('click');
				$('.finally-form .radios').addClass('disabled');
				break;
			case 'courier':
				$('.finally-form input.pay1').trigger('click');
				$('.finally-form .radios').addClass('disabled');
				break;
			default:
				$('.finally-form input.pay1').prop("checked",false);
				$('.finally-form .radios').removeClass('disabled');
			break;
		}
	});



	$('.finally-form fieldset').on('click', function(){
		if($(this).hasClass('finished')){
			$('.finally-form fieldset').removeClass('active');
			$(this).addClass('active');
			$(this).removeClass('finished');
			$(this).prev().addClass('finished');
		}
		$('.finally-form .active.first .btn-holder .btn').val('Далее');
		$('.finally-form .finished.first .btn-holder .btn').val('Изменить данные');
		$('.finally-form .active.second .btn-holder .btn').val('Далее');
		$('.finally-form .finished.second .btn-holder .btn').val('Изменить данные');
	});
	$('#nav li.has-drop .float-holder a.title').on('click', function(){
		$(this).next('.drop-list').toggleClass('open');
		return false;
	});
	$('.footer-list .float-holder a.title').on('click', function(){
			$(this).next('.column-list').toggleClass('open');
		return false;
	});
	$('.card-form .input-holder .number').mask("+7 (999) 999-99-99");



	$('.card-section .top-holder .info-card.general .link-holder .link').on('click', function(){
		$('.card-section .info-card').removeClass('active');
		$('.card-section .info-card.step2').addClass('active');
		return false;
	});
	$('.card-section .top-holder .info-card.general p a').on('click', function(){
		$('.card-section .info-card').removeClass('active');
		$('.card-section .info-card.step3').addClass('active');
		return false;
	});

	$('.card-section .top-holder .info-card .close, .card-form .link, .info-card.accept .link-holder .link').on('click', function(){
		$('.card-section .top-holder .info-card.step2').removeClass('active');
		$('.card-section .top-holder .info-card.step3').removeClass('active');
		$('.card-section .top-holder .info-card.step4').removeClass('active');
		$('.card-section .top-holder .info-card.step1').addClass('active');
		return false;
	});

	$('.card-section .info-card .btn.to_accept').on('click', function(){
		$('.card-form .input-holder .number').blur();
		if($('.card-form .input-holder .number').hasClass('error')){}
		else {
			$('.card-section .info-card').removeClass('active');
			$('.card-section .info-card.step4').addClass('active');
		}
		return false;
	});

});

$(window).resize(function(){
		initSly();
		$('.catalog-list li').matchHeight();
	});

$(window).load(function(){
	initSly();
	$('.catalog-list li').matchHeight();
	$('.partners-section .slide a').matchHeight();
	setTimeout(function() {
		$('.partners-section .slide a').css('line-height', function(){
			return $(this).height() + 'px';
		})
	}, 5);
});


/*inpty[type="number"]*/
function initCountInput(){
	$('.count-input a').click(function(){
		var _curVal = $(this).closest('.count-input').find('input').val();
		if($(this).hasClass('plus')){
			_curVal++;
			$(this).closest('.count-input').find('.minus').removeClass('disabled');
		} else {
			if(_curVal > 0){
				_curVal--;
			} else {
				$(this).addClass('disabled');
			}
			if(_curVal == 1){
				$(this).addClass('disabled');
			}
		}
		$(this).closest('.count-input').find('input').val(_curVal).trigger('change');
		return false;
	})
}
/*inpty[type="number"]*/

function maskInput() {
	if ($(".input.number").size()) $(".input.number").mask("+7 (999) 999-99-99", {
		placeholder: "_"
	});
	if ($(".input.pasport").size()) $(".input.pasport").mask("99 99 999999", {
		placeholder: "_"
	});
}

function initPlayerForm(){
	$('form').each(function(){
      var form=$(this),
      input=form.find('input:text');
  		form.find('.required').blur(function(){
            var val=$(this).val();
            if((/^[а-яА-ЯіІєЄїЇ\s-]{1,40}$/ig).test(val)){
                $(this).closest('.row').removeClass('error');
                $(this).removeClass('error');
            }
            else{
                $(this).closest('.row').addClass('error');
                $(this).addClass('error');
            }
        });
        form.on('keyup keydown', '.required.error', function(){
            var val=$(this).val();
            if((/^[a-zA-Z0-9а-яА-ЯіІєЄїЇ\s-\(\)\+]{1,40}$/ig).test(val)){
                $(this).closest('.row').removeClass('error');
                $(this).removeClass('error');
            }
            else{
                $(this).closest('.row').addClass('error');
                $(this).addClass('error');
            }
        });
		 form.find('.number').blur(function(){
            var val=$(this).val();
             if((/^[0-9\s-\(\)\+]{18}$/ig).test(val)){
                $(this).closest('.row').removeClass('error');
                $(this).removeClass('error');
            }
            else{
                $(this).closest('.row').addClass('error');
                $(this).addClass('error');
            }
        });
        form.on('keyup keydown', '.number.error', function(){
            var val=$(this).val();
            if((/^[0-9\s-\(\)\+]{18}$/ig).test(val)){
                $(this).closest('.row').removeClass('error');
                $(this).removeClass('error');
            }
            else{
                $(this).closest('.row').addClass('error');
                $(this).addClass('error');
            }
        });
      form.find('.emailv').blur(function(){
          var val=$(this).val();
          if((/^[-\._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/ig).test(val) && val.length<=30){
              $(this).closest('.row').removeClass('error');
              $(this).removeClass('error');
          }
          else{
              $(this).closest('.row').addClass('error');
              $(this).addClass('error');
          }
      });
      form.on('keyup keydown', '.emailv.error', function(){
          var val=$(this).val();
          if((/^[-\._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/ig).test(val) && val.length<=30){
              $(this).closest('.row').removeClass('error');
              $(this).removeClass('error');
          }
          else{
              $(this).closest('.row').addClass('error');
          }
      });
      form.submit(function(e){
          input.trigger('blur');
          if(form.find('.error').size()){
			/*alert('error');*/
			return false;
		} else {
			// $.post("feedback.php", $(this).serialize());
			values = $(this).serialize();
			console.log();
			$.ajax({
				url: "feedback.php",
				type: "post",
				data: values,
				success: function(){
					// успех
					form.find('input[type=text]').each(function() {
						$(this).val('');
					});
					$('.close-popup').click();
					alert('Спасибо! Заявка принята.')
					
				},
				error:function(){
					// ошибка
				}
			});
			return false;
		}
		});
	});
};







// Sly start

	function initSly(){
		var $frame1 = $('.gallery-cover');
		$frame1.each(function(){
			var $wrap1 = $(this).closest('.gallery');
			$(this).sly({
				horizontal: 1,
				itemNav: 'forceCentered',
				smart: 1,
				startAt: 0,
				activateMiddle: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				scrollBy:0,
				speed: 500,
				elasticBounds: 1,
				easing: 'swing',
				dragHandle: 1,
			 	dynamicHandle: 1,
				clickBar: 1,
				elasticBounds: 1,
				prevPage: $wrap1.find('.prev'),
				nextPage: $wrap1.find('.next')
			});
			var _this = $(this);
			$(this).sly('on', 'active',function(){
				var itemIndex = _this.find('li.active').index();
				_this.closest('.gallery').find('.switcher-list li').eq(itemIndex).click();
			});
			$frame1.sly('reload');
		});
		var $frame2 = $('.switcher-cover');
		$frame2.each(function(){
			var $wrap2 = $(this).closest('.switcher');
			$(this).sly({
				horizontal: 1,
				itemNav: 'basic',
				smart: 1,
				startAt: 0,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				scrollBy:1,
				speed: 500,
				elasticBounds: 1,
				easing: 'swing',
				dragHandle: 1,
			 	dynamicHandle: 1,
				clickBar: 1,
				prevPage: $wrap2.find('.prev'),
				nextPage: $wrap2.find('.next')
			});
			$(this).find('li').unbind( "click" ).click(function(){
				var index = $(this).index();
				$(this).closest('.gallery').find('.gallery-holder li').unbind( "click" ).eq(index).click();
			});
			$frame2.sly('reload');
		});
	};

// Sly END






function initMap(){
					if(!$('#map_yandex').size())return;
						ymaps.ready(init);
						function init() {
							myMap = new ymaps.Map("map_yandex", {
							center: [45.038184, 39.085878],
							zoom: 11.5
						});
						myPlacemark = new ymaps.Placemark([45.038184, 39.085878], {}, {
							iconImageHref: 'images/map-logo.png',
							iconImageSize: [32, 41],
							iconImageOffset: [-8, -94]
						});
						myPlacemark2 = new ymaps.Placemark([45.037795, 38.953457], {}, {
							iconImageHref: 'images/map-logo.png',
							iconImageSize: [32, 41],
							iconImageOffset: [-8, -94]
						});
						myMap.geoObjects.add(myPlacemark);
						myMap.geoObjects.add(myPlacemark2);
						myMap.controls.add('zoomControl', {left: '10px', top: '50px'});
						myMap.controls.add('typeSelector', {right: '110px', top: '10px'});
						myMap.controls.add('trafficControl', {right: '10px', top: '10px'});
						myMap.controls.add('mapTools', {left: '10px', top: '10px'});
					}
				}


function initTabs(){
	$('.tabset .tab-control a').on('click', function(){
		var thisHold = $(this).closest(".tabset");
		var _ind = $(this).closest('li').index();
		thisHold.children('.tab-body').children(".tab").removeClass('active');
		thisHold.children('.tab-body').children("div.tab:eq("+_ind+")").addClass('active');
		$(this).closest("ul").find(".active").removeClass("active");
		$(this).parent().addClass("active");
		return false;
	});
};

function initTabsForm(){
	$('.tabsetf .tab-control input').on('click', function(){
		var thisHold = $(this).closest(".tabsetf");
		var _ind = $(this).closest('div').index();
		thisHold.children('.tab-body').children(".tab").removeClass('active');
		thisHold.children('.tab-body').children("div.tab:eq("+_ind+")").addClass('active');
		$(this).closest("ul").find(".active").removeClass("active");
		$(this).parent().addClass("active");
	});
};

$.fn.popup = function(o){
		var o = $.extend({
			"opener":".call-back a",
			"popup_holder":"#call-popup",
			"popup":".popup",
			"close_btn":".btn-close",
			"close":function(){},
			"beforeOpen": function(popup) {
			 $(popup).css({
				'left': 0,
				'top': 0
			 }).hide();
		}
	},o);
	return this.each(function(){
		var container=$(this),
		opener=$(o.opener,container),
		popup_holder=$(o.popup_holder,container),
		popup=$(o.popup,popup_holder),
		close=$(o.close_btn,popup),
		bg=$('.bg',popup_holder);
		popup.css('margin',0);
		opener.click(function(e){
			o.beforeOpen.apply(this,[popup_holder]);
			popup_holder.css('left',0);
			popup_holder.fadeIn(400);
			alignPopup();
			bgResize();
			e.preventDefault();
		});
		function alignPopup(){
			var deviceAgent = navigator.userAgent.toLowerCase();
			var agentID = deviceAgent.match(/(iphone|ipod|ipad|android)/i);
			if(agentID){
				if(popup.outerHeight()>window.innerHeight){
					popup.css({'top':$(window).scrollTop(),'left': ((window.innerWidth - popup.outerWidth())/2) + $(window).scrollLeft()});
				return false;
				}
				popup.css({
					'top': ((window.innerHeight-popup.outerHeight())/2) + $(window).scrollTop(),
					'left': ((window.innerWidth - popup.outerWidth())/2) + $(window).scrollLeft()
				});
				} else {
					if(popup.outerHeight()>$(window).outerHeight()){
						popup.css({'top':$(window).scrollTop(),'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()});
						return false;
					}
				popup.css({
					'top': (($(window).height()-popup.outerHeight())/2) + $(window).scrollTop(),
					'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()
				});
			}
		}
		function bgResize(){
			var _w=$(window).width(),
			_h=$(document).height();
			bg.css({"height":_h,"width":_w+$(window).scrollLeft()});
		}
		$(window).resize(function(){
			if(popup_holder.is(":visible")){
				bgResize();
				alignPopup();
			}
		});
		if(popup_holder.is(":visible")){
			bgResize();
			alignPopup();
		}
		close.add(bg).click(function(e){
			var closeEl=this;
			popup_holder.fadeOut(400,function(){
				o.close.apply(closeEl,[popup_holder]);
			});
			e.preventDefault();
		});
		$('body').keydown(function(e){
			if(e.keyCode=='27'){
				popup_holder.fadeOut(400);
			}
		})
	});
};